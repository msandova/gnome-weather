# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-weather master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-weather/issues\n"
"POT-Creation-Date: 2021-02-28 12:52+0000\n"
"PO-Revision-Date: 2021-06-16 19:57+0545\n"
"Last-Translator: Pawan Chitrakar <chautari@gmail.com>\n"
"Language-Team: Nepali Translation Team <chautari@gmail.com>\n"
"Language: ne_NP\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2;plural=(n!=1);\n"
"X-Generator: Poedit 1.8.4\n"

#: data/org.gnome.Weather.appdata.xml.in.in:5 data/org.gnome.Weather.desktop.in.in:4
#: data/window.ui:89 src/app/main.js:64 src/app/window.js:203 src/service/main.js:48
msgid "Weather"
msgstr "मौसम"

#: data/org.gnome.Weather.appdata.xml.in.in:6 data/org.gnome.Weather.desktop.in.in:5
msgid "Show weather conditions and forecast"
msgstr "हालको मौसम अवस्था अनुगमन गर्दछ, र पूर्वानुमान गर्दछ"

#: data/org.gnome.Weather.appdata.xml.in.in:16
msgid ""
"A small application that allows you to monitor the current weather conditions for your "
"city, or anywhere in the world."
msgstr ""
"एउटा सानो अनुप्रयोगले तपाईंलाई तपाईंको शहर, वा संसारको कुनै पनि ठाउँमा मौसमको सर्त निगरानी गर्न "
"अनुमति दिन्छ।"

#: data/org.gnome.Weather.appdata.xml.in.in:20
msgid ""
"It provides access to detailed forecasts, up to 7 days, with hourly details for the "
"current and next day, using various internet services."
msgstr ""
"यसले विभिन्न इन्टरनेट सेवाहरू प्रयोग गरी वर्तमान र अर्को दिनको लागि घटेको विवरणहरूको साथ, ७ "
"दिनसम्म विस्तृत पूर्वानुमानहरूको पहुँच प्रदान गर्दछ।"

#: data/org.gnome.Weather.appdata.xml.in.in:24
msgid ""
"It also optionally integrates with the GNOME Shell, allowing you to see the current "
"conditions of the most recently searched cities by just typing the name in the "
"Activities Overview."
msgstr ""
"यो पनि वैकल्पिक रूपमा जिनोम शैलसँग एकीकृत हुन्छ, तपाईंलाई हालको खोजी सहरहरूको अवस्था र क्रियाकलाप "
"अवलोकनमा यसको नाम टाइप गरेर हालको अवस्था हेर्न अनुमति दिन्छ।"

#: data/org.gnome.Weather.appdata.xml.in.in:119
msgid "The GNOME Project"
msgstr "जिनोम परियोजना"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Weather.desktop.in.in:13
msgid "Weather;Forecast;"
msgstr "मौसम;पूर्वानुमान;"

#: data/org.gnome.Weather.desktop.in.in:14
msgid "Allows weather information to be displayed for your location."
msgstr "तपाईंको स्थानको लागि मौसम जानकारी प्रदर्शन गर्न अनुमति दिन्छ।"

#: data/org.gnome.Weather.gschema.xml:6
msgid "Configured cities to show weather for"
msgstr "मौसम देखाउन कन्फिगर गरिएको शहरहरू"

#: data/org.gnome.Weather.gschema.xml:7
msgid ""
"The locations shown in the world view of gnome-weather. Each value is a GVariant "
"returned by gweather_location_serialize()."
msgstr ""
"संसारमा देखिएका ठाउँहरू ग्नम-मौसमको दृश्य हेर्नुहोस्। प्रत्येक मान GVariant "
"gweather_location_serialize() द्वारा फर्काइएको हो।"

#: data/org.gnome.Weather.gschema.xml:14
msgid "Automatic location"
msgstr "स्वत: स्थान"

#: data/org.gnome.Weather.gschema.xml:15
msgid ""
"The automatic location is the value of automatic-location switch which decides whether "
"to fetch current location or not."
msgstr ""
"स्वचालित स्थान स्वचालित-स्थान स्विचको मान हो जसले निर्णय गर्दछ कि वर्तमान स्थान ल्याउन वा छैन।"

#: data/city.ui:9
msgid "City view"
msgstr "शहरी दृश्य"

#: data/city.ui:32
msgid "Loading…"
msgstr "लोड गर्दै…"

#: data/day-entry.ui:26
msgid "Night"
msgstr "रात"

#: data/day-entry.ui:41
msgid "Morning"
msgstr "बिहान"

#: data/day-entry.ui:56
msgid "Afternoon"
msgstr "दिउँसो"

#: data/day-entry.ui:71
msgid "Evening"
msgstr "साँझ"

#: data/places-popover.ui:46
msgid "Automatic Location"
msgstr "स्वत: स्थान"

#: data/places-popover.ui:85
msgid "Locating…"
msgstr "फेला पार्दै…"

#: data/places-popover.ui:142
msgid "Search for a city"
msgstr "शहर खोज्नुहोस्"

#: data/places-popover.ui:174
msgid "Viewed Recently"
msgstr "हालै हेरिसकेको"

#: data/primary-menu.ui:4 data/window.ui:7
msgid "_Temperature Unit"
msgstr "तापक्रम एकाइ"

#: data/primary-menu.ui:6 data/window.ui:9
msgid "_Celsius"
msgstr "सेल्सियस"

#: data/primary-menu.ui:11 data/window.ui:14
msgid "_Fahrenheit"
msgstr "फरेनहाइट"

#: data/primary-menu.ui:19 data/window.ui:22
msgid "_About Weather"
msgstr "मौसम बारेमा"

#: data/weather-widget.ui:73
msgid "Places"
msgstr "स्थानहरू"

#: data/weather-widget.ui:149
msgid "Current conditions"
msgstr "हालको अवस्था"

#: data/weather-widget.ui:180
msgid "Hourly"
msgstr "घण्टा घण्टामा"

#: data/weather-widget.ui:201
msgid "Daily"
msgstr "दैनिक"

#: data/window.ui:51
msgid "Refresh"
msgstr "ताजा पार्नुहोस्"

#: data/window.ui:77
msgid "Select Location"
msgstr "_स्थान छान्नुहोस्"

#: data/window.ui:126
msgid "Welcome to Weather!"
msgstr "मौसम स्वागत!"

#: data/window.ui:127
msgid "To get started, select a location."
msgstr "सुरु गर्न, स्थान चयन गर्नुहोस् ।"

#: data/window.ui:136
msgid "Search for a city or country"
msgstr "सहर वा देशको खोजी गर्नुहोस्"

#: src/app/city.js:209
#, javascript-format
msgid "Feels like %.0f°"
msgstr "%.0f° जस्तो लाग्छ"

#: src/app/city.js:241
msgid "Updated just now."
msgstr "अहिले अद्यावधिक गरियो ।"

#: src/app/city.js:246
#, javascript-format
msgid "Updated %d minute ago."
msgid_plural "Updated %d minutes ago."
msgstr[0] "%d मिनेट पहिले अद्यावधिक गरियो ।"
msgstr[1] "%d मिनेट पहिले अद्यावधिक गरियो ।"

#: src/app/city.js:252
#, javascript-format
msgid "Updated %d hour ago."
msgid_plural "Updated %d hours ago."
msgstr[0] "%d घण्टा पहिले अद्यावधिक गरियो ।"
msgstr[1] "%d घण्टा पहिले अद्यावधिक गरियो ।"

#: src/app/city.js:258
#, javascript-format
msgid "Updated %d day ago."
msgid_plural "Updated %d days ago."
msgstr[0] "%d दिन पहिले अद्यावधिक गरियो ।"
msgstr[1] "%d दिन पहिले अद्यावधिक गरियो ।"

#: src/app/city.js:264
#, javascript-format
msgid "Updated %d week ago."
msgid_plural "Updated %d weeks ago."
msgstr[0] "%d हप्ता पहिले अद्यावधिक गरियो ।"
msgstr[1] "%d हप्ता पहिले अद्यावधिक गरियो ।"

#: src/app/city.js:269
#, javascript-format
msgid "Updated %d month ago."
msgid_plural "Updated %d months ago."
msgstr[0] "%d महिना पहिले अद्यावधिक गरियो ।"
msgstr[1] "%d महिना पहिले अद्यावधिक गरियो ।"

#: src/app/dailyForecast.js:36
msgid "Daily Forecast"
msgstr "दैनिक मौसम पूर्वानुमान"

#: src/app/dailyForecast.js:93 src/app/hourlyForecast.js:89
msgid "Forecast not available"
msgstr "पूर्वानुमान उपलब्ध छैन"

#. Translators: this is the time format for day and month name according to the current locale
#: src/app/dailyForecast.js:179
msgid "%b %e"
msgstr "%b %e"

#: src/app/hourlyForecast.js:40
msgid "Hourly Forecast"
msgstr "घण्टा घण्टामा मौसम पूर्वानुमान"

#. Translators: this is a time format without date used for AM/PM
#: src/app/hourlyForecast.js:107
msgid "%l∶%M %p"
msgstr "%l∶%M %p"

#: src/app/window.js:202
msgid "translator-credits"
msgstr "Pawan Chitrakar < chautari@gmail.com >"

#: src/app/window.js:204
msgid "A weather application"
msgstr "मौसम अनुप्रयोग"

#: src/app/world.js:38
msgid "World view"
msgstr "विश्व दृश्य"

#. TRANSLATORS: this is the temperature string, minimum and maximum.
#. The two values are already formatted, so it would be something like
#. "7 °C / 19 °C"
#: src/misc/util.js:140
#, javascript-format
msgid "%s / %s"
msgstr "%s / %s"

#. TRANSLATORS: this is the description shown in the overview search
#. It's the current weather conditions followed by the temperature,
#. like "Clear sky, 14 °C"
#: src/service/searchProvider.js:182
#, javascript-format
msgid "%s, %s"
msgstr "%s, %s"

#~ msgid "_New"
#~ msgstr "नयाँ"

#~ msgid "About"
#~ msgstr "बारेमा"

#~ msgid "Quit"
#~ msgstr "अन्त्य गर्नुहोस्"

#~ msgid "Today"
#~ msgstr "आज"

#~ msgid "Tomorrow"
#~ msgstr "भोलि"

#~ msgid "Search for a location"
#~ msgstr "स्थानको लागि खोजि गर्नुहोस्"

#~ msgid "To see weather information, enter the name of a city."
#~ msgstr "मौसम जानकारी हेर्न, सहरको नाम प्रविष्ट गर्नुहोस्।"

#~ msgid "Forecast"
#~ msgstr "पूर्वानुमान"

#~ msgid "%R"
#~ msgstr "%R"

#~ msgid "%A"
#~ msgstr "%A"

#~ msgid "%H:%M"
#~ msgstr "%H:%M"
